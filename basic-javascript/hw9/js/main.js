"use strict";

const tabs = document.querySelector(".tabs");
const tabNav = document.querySelectorAll(".tabs-title");
const tabText = document.querySelectorAll(".tab");

tabs.addEventListener("click", function(event) {
tabNav.forEach(item => {
    item.classList.remove("active")
})
    event.target.classList.add("active");
tabText.forEach(item => {

    if (event.target.dataset.name === item.dataset.text) {
        item.classList.add("active-tab");
    } else {
        item.classList.remove("active-tab");
    }
})
})
