"use strict";

const input = document.getElementById("number");
const price = document.getElementById("price");
const error = document.getElementById("error");


input.onfocus = function() {
    input.classList.add("active");

    if (input.classList.contains("error")) {
        input.classList.remove("error");
        error.textContent = "";
    };
};

input.onblur = function() {

    if (input.value <= 0) {
        error.textContent = "Please enter correct price";
        input.classList.add("error");
    } else {
        const element = document.createElement ("span");
        element.innerHTML = `Текущая цена: ${input.value}`;
        price.appendChild(element);
        input.style.color = "green";

        const button = document.createElement("button");
        button.textContent = "x";
        element.after(button);
        button.addEventListener("click", function() {
           element.remove();
           button.remove();
           input.value = "";
       });
    };
}




