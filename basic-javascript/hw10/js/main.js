"use strict";

const text = document.getElementById("text");
const pass = document.getElementById("pass");
const icons = document.querySelectorAll(".fas");
const inputArea = document.querySelectorAll("input");
const btn = document.querySelector(".btn");

for (let i = 0; i < icons.length; i++) {
    icons[i].addEventListener("click", function(event) {
        if (event.target.classList.contains("fa-eye")) {
            event.target.classList.remove("fa-eye");
            event.target.classList.toggle("fa-eye-slash");
            inputArea[i].type = "password"
        } else {
            event.target.classList.remove("fa-eye-slash");
            event.target.classList.toggle("fa-eye");
            inputArea[i].type = "text";
        }
    })
}

btn.addEventListener("click", function(event) {
    event.preventDefault();
    if (text.value === pass.value) {
        alert("You are welcome!")
    } else {
        let span = document.createElement("span");
        span.textContent = "Нужно ввести одинаковые значения";
        span.style.color = "red";
        document.body.append(span);
    }
})

