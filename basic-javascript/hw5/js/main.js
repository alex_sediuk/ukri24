"use strict";

function createNewUser() {

    const newUser = {
        getLogin: function() {
            return (this.Name[0].toLowerCase() + this.lastName.toLowerCase());
        },
        getAge: function() {
            let now = new Date();
            let currentYear = now.getFullYear();

            const inputDate = +this.birthday.substring(0, 2);
            const inputMonth = +this.birthday.substring(3, 5);
            const inputYear = +this.birthday.substring(6, 10);

            let birthDate = new Date(inputYear, inputMonth-1, inputDate);
            let birthYear = birthDate.getFullYear();
            let age = (currentYear - birthYear);

            if (now < new Date(birthDate.setFullYear(currentYear))) {
                age = age - 1;
            }
            return (age);
        },
        getPassword: function() {

            return (this.Name[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday.substring(6, 10));
        }
    };
    newUser.Name = prompt("Введите Имя");
    newUser.lastName = prompt("Введите Фамилию");
    newUser.birthday = prompt("Введите дату рождения dd.mm.yyyy");
    newUser.getAge();
    newUser.getPassword();

    return newUser;
};

createNewUser();