"use strict";

const img = document.querySelectorAll(".img-to-show");
let slider = setInterval(imgSlider, 10000);
let counter = 0;

function imgSlider() {

    img[counter].classList.remove("active");
    counter++;

    if (counter === img.length) {
        counter = 0
    }
    img[counter].classList.add("active");

    let stop =  document.getElementById("btn");

    stop.addEventListener("click", function(event) {
    clearInterval(slider);
    });

    let start = document.getElementById("cont_btn");

    start.addEventListener("click", function(event) {
        slider = setInterval(imgSlider, 10000);
    });
}




