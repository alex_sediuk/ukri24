"use strict";

$(function () {
    const $subitem = $(".subitem");

    $('.top_menu').on("click", "li", function () {
        /* Hide all .subitem */
        $subitem.toggle("slow");
        /* Show children .subitem */
        $(this).children(".subitem").slideDown("slow");
        $subitem.show();

    });
});




$(document).ready(function(){
    $('body').append('<a href="#" id="go-top" title="Вверх">Вверх</a>');
});

$(function() {
    $.fn.scrollToTop = function() {
        $(this).hide().removeAttr("href");
        if ($(window).scrollTop() >= "250") $(this).fadeIn("slow")
        let scrollDiv = $(this);
        $(window).scroll(function() {
            if ($(window).scrollTop() <= "250") $(scrollDiv).fadeOut("slow")
            else $(scrollDiv).fadeIn("slow")
        });
        $(this).click(function() {
            $("html, body").animate({scrollTop: 0}, "slow")
        })
    }
});

$(function() {
    $("#go-top").scrollToTop();
});



$(document).ready(function(){
    $(".subscribe").click(function(){
        $("#panel").slideToggle("slow");
        $(this).toggleClass("active");
    });
});

