"use strict";

function newArr (arr) {

    const ul = document.createElement("ul");
    const list = arr.map((value) => `<li>${value}</li>`)

    console.log(list);

    ul.insertAdjacentHTML("beforeend", list.join(""));
    document.body.append(ul);

};
newArr(['hello', 'world', 'Kyiv', 'Kharkiv', 'Odesa', 'Lviv'])

