"use strict";

if(localStorage.color) {
    document.body.style.backgroundColor = localStorage.color;

};
function changeBackground() {
    let currentValue = localStorage.color || 'white';
    currentValue = currentValue == 'white' ? 'orange' : 'white';
    localStorage.setItem('color', document.body.style.backgroundColor = currentValue);
};

if(localStorage.btn) {
    document.querySelector(".start").style.backgroundColor = localStorage.btn;
};
function changeButton() {
    let currentValue = localStorage.btn || 'green';
    currentValue = currentValue == 'green' ? 'red' : 'green';
    localStorage.setItem('btn', document.querySelector(".start").style.backgroundColor = currentValue);
};
